using System.Text;

namespace PasswordGenerator.Models;

public class Password {
  public static bool CheckValidity(string password) {
    if (password.Any(char.IsUpper) &&
      password.Any(char.IsLower) &&
      password.Any(char.IsDigit) &&
      password.Length >= 8) {
      return true;
    }
    return false;
  }

  public static List<string> Generate(int count) {
    List<string> passwords = new List<string>();
    string charsUpper = "ABCDEFGHIJKLMNOPQRSTUVXYZ";
    string charsLower = "abcdefghijklmnopqrstuvxyz";
    string digits = "0123456789";
    string symbols = "$!#&+-.,;";

    string validChars = charsUpper + charsLower + digits + symbols;

    var random = new Random();
    while (count > 0) {
      string password = "";
      for (int i = 0; i < 8; i++) {
        password += validChars[random.Next(validChars.Length-1)];
      }
      if (!CheckValidity(password.ToString())) {
        continue;
      }
      passwords.Add(password.ToString());
      count--;
    }
    return passwords;
  }
}