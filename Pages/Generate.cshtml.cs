using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PasswordGenerator.Models;

namespace PasswordGenerator.Pages {
  public class GenerateModel : PageModel {
    public void OnGet() {
      Passwords = Password.Generate(10);
    }

    public List<string>? Passwords { get; set; }
  }
}
