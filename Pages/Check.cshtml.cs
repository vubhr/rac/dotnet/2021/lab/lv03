using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PasswordGenerator.Models;

namespace PasswordGenerator.Pages {
  public class CheckModel : PageModel {
    public CheckModel() {
      Message = "";
    }
    public void OnGet(string password) {
      if (password != null) {
        if (Password.CheckValidity(password)) {
          Message = "Password is valid";
        } else {
          Message = "Password is invalid";
        }
      }
    }

    public string Message { get; set; }
  }
}
